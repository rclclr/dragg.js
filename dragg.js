/* global jQuery:false */

// need to handle events outside the div....
// on mousemove and on mouseout in the body...

(function ($) {
	var init = true;
	var idx = 0;

$.fn.draggable = function(options) {
	if (!options) {
		options = {
			"restricted": "true",
			"moveToTop": "true",
		};
	}
	if (!options.onMove) {
		options.onMove = function () { return; };
	}

	if (init) {
		init = false;
		// Attach event handlers to #dragg-curtain for mouse movement
		// #dragg-curtain only exists after a startMove event...
		$("body")
			.on("mousemove", "#dragg-curtain", dragObject)
			.on("mousemove", "#dragg-curtain", triggerContainerCallback)
			.on("mouseup",   "#dragg-curtain", stopMove);
	}

	// Attach event handlers to movable objects
	return this.not("[data-dragg-draggable=true]") // Only process new objects
		.css({
			"position": "absolute",
			"box-sizing": "border-box"
		})
		.attr({
			"data-dragg-draggable": "true",
			"data-dragg-moving": "false",
			"data-dragg-restricted": (options.restricted ? "true" : "false")
		})
		.each(function() {
			$(this)
				.on("mousedown",  startMove)
				.on("touchstart", startMove)
				.on("touchend",   stopMoveTouch)
				.on("touchmove",  dragObjectTouch)
				.on("touchmove",  triggerContainerCallbackTouch)
				.parent().not("[data-dragg-container]")	// Only process container once
					.attr("data-dragg-container", idx)		// Tag it with an ID number
					.parentsUntil("html").last()					// Attach custom onMove event handler to body
						.on("dragg-drag-" + idx, function (event, obj) {
							options.onMove(event, obj);
						});

			idx += 1;

			// Handle option to bring object to top of stack
			var top = +$(this).parent().attr("data-dragg-top") || 0;
			if (options.moveToTop) {
				$(this).css({"z-index": (options.moveToTop ? top : "auto")})
					.parent().attr({
						"data-dragg-move-to-top": "true",
						"data-dragg-top": (top ? top + 1 : 1)
				});
			}
	});
};

$.fn.getCenterPosition = function() {
	return {
		"left": +$(this).position().left + $(this).width()/2,
		"top":  +$(this).position().top  + $(this).height()/2
	};
};

$.fn.findNeighbors = function(maxDist, handlerNeighbors, handlerOthers) {
	var neighbors = $();
	this.each(function() {
			var obj = $(this);
			$(this).parent().children("[data-dragg-moving=false]").each(function() {
					var dist = $(this).distanceTo(obj);
				if (dist <= maxDist) {
					neighbors = neighbors.add($(this));
					if (handlerNeighbors) {
						handlerNeighbors($(this));
					}
				}
				else if (handlerOthers) {
					handlerOthers($(this));
				}
			});
	});
	return neighbors;
};

$.fn.distanceTo = function(obj) {
	var c1 = this.getCenterPosition();
	var c2 = obj.getCenterPosition();
	return Math.sqrt(Math.pow(c1.left-c2.left, 2) + Math.pow(c1.top-c2.top, 2));
};

function triggerContainerCallback() {
	// The handler is called by #dragg-curtain
	var id = $("[data-dragg-moving=true]").parent().attr("data-dragg-container");
	$("body").trigger("dragg-drag-" + id, [$("[data-dragg-moving=true]")]);
}

function triggerContainerCallbackTouch() {
	// The handler is called by the moved object itself
	var id = $(this).parent().attr("data-dragg-container");
	$("body").trigger("dragg-drag-" + id, [$("[data-dragg-moving=true]")]);
}

function stopMove() {
	// There is only one moving object...
	$("[data-dragg-moving=true]")
		.css({ "cursor": "default" })
		.attr({
			"data-dragg-moving": false,
			"data-dragg-dX": null,
			"data-dragg-dY": null,
		});
	$("#dragg-curtain").remove();
}

function stopMoveTouch() {
	$(this).attr("data-dragg-moving", false);
}

function startMove(event) {
	// TODO: Check if there are other tags that need to be detected...
	var notExcluded = $.inArray(event.target.tagName, ["SELECT", "INPUT", "BUTTON", "TEXTAREA", ""]) === -1;
	var notDisabled = $(event.target).attr("data-dragg-disable") !== "true";
	if (notExcluded && notDisabled) {
		event.preventDefault();
		$(this)
			.css({ "cursor": "none" })
			.attr("data-dragg-moving", true);

		var e;
		if (event.originalEvent.targetTouches) {
			e = event.originalEvent.targetTouches[0];
		} else {
			// Add the #dragg-curtain div on top that will catch mousemove events
			$("<div id='dragg-curtain'></div>")
				.css({
					"top":  0, "bottom":  0,
					"left": 0, "right":   0,
					"position": "fixed",
					"z-index":  10000,
					// "background-color": "red", "opacity": 0.5
			})
				.appendTo($("body"));
			e = event;
		}
		// Mouse position position
		var dX = e.pageX - $(this).offset().left - $(this).width()/2;
		var dY = e.pageY - $(this).offset().top  - $(this).height()/2;
		$(this).attr({ "data-dragg-dX": dX, "data-dragg-dY": dY });
	}
	// Move object to top of the stack
	if ($(this).parent().attr("data-dragg-move-to-top") === "true") {
		var z = +$(this).css("z-index");
		var sib = $(this).siblings("[data-dragg-draggable]");
		sib.sort(zSort);
		sib.each(function() {
			if ($(this).css("z-index") > z) {
				$(this).css("z-index", +$(this).css("z-index") - 1);
			}
		});
		$(this).css("z-index", sib.length);
	}
}

function dragObject(event) {
	var dot = $("[data-dragg-moving=true]");

	// Get new object position from mouse position
	var offX = 0;
	var offY = 0;
	var offMX = 0;
	var offMY = 0;
	if (dot.parent().css("position") === "absolute") {
		offX = dot.parent().offset().left;
		offY = dot.parent().offset().top;
	} else {
		offMX = dot.parent().offset().left;
		offMY = dot.parent().offset().top;
	}
	var X = event.pageX - offX - dot.attr("data-dragg-dX");
	var Y = event.pageY - offY - dot.attr("data-dragg-dY");

	if (dot.attr("data-dragg-restricted") === "true") {
		var minDotX = dot.width()/2  + offMX;
		var minDotY = dot.height()/2 + offMY;
		var maxDotX = dot.parent().width()  - dot.width()/2  + offMX;
		var maxDotY = dot.parent().height() - dot.height()/2 + offMY;
		X = (X < minDotX) ? minDotX : X;
		Y = (Y < minDotY) ? minDotY : Y;
		X = (X > maxDotX) ? maxDotX : X;
		Y = (Y > maxDotY) ? maxDotY : Y;
	}
	dot.css({ "left": (X - dot.width()/2) + "px", "top": (Y - dot.height()/2) + "px" });
}

function dragObjectTouch(event) {
	var touch = event.originalEvent.targetTouches[0];
	var dot = $(touch.target);

	// Get new object position from finger position
	var offX = 0;
	var offY = 0;
	var offMX = 0;
	var offMY = 0;
	if (dot.parent().css("position") === "absolute") {
		offX = dot.parent().offset().left;
		offY = dot.parent().offset().top;
	} else {
		offMX = dot.parent().offset().left;
		offMY = dot.parent().offset().top;
	}
	var X = touch.pageX - offX - dot.attr("data-dragg-dX");
	var Y = touch.pageY - offY - dot.attr("data-dragg-dY");

	if (dot.attr("data-dragg-restricted") === "true") {
		var minDotX = dot.width()/2  + offMX;
		var minDotY = dot.height()/2 + offMY;
		var maxDotX = dot.parent().width()  - dot.width()/2  + offMX;
		var maxDotY = dot.parent().height() - dot.height()/2 + offMY;
		X = (X < minDotX) ? minDotX : X;
		Y = (Y < minDotY) ? minDotY : Y;
		X = (X > maxDotX) ? maxDotX : X;
		Y = (Y > maxDotY) ? maxDotY : Y;
	}
	dot.css({ left: (X - dot.width()/2) + "px", top: (Y - dot.height()/2) + "px" });
}

function zSort(a, b) {
	var an = +$(a).css("z-index");
	var bn = +$(b).css("z-index");
	if (an > bn) {
		return 1;
	}
	if (an < bn) {
		return -1;
	}
	return 0;
}

}( jQuery ));
